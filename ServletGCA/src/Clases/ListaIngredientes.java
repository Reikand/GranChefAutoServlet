package Clases;

import java.util.Map;

/**
 * Created by Ruben on 04/05/2016.
 */
public class ListaIngredientes {

    private Map<String, Integer> listIngredients;

    public ListaIngredientes(Map<String, Integer> listIngredients) {
        this.listIngredients = listIngredients;
    }

    public Map<String, Integer> getListIngredients() {
        return listIngredients;
    }

    public void setListIngredients(Map<String, Integer> listIngredients) {
        this.listIngredients = listIngredients;
    }
}

package Clases;

import java.io.Serializable;
import java.util.Map;

import com.google.gson.annotations.JsonAdapter;

/**
 * Created by iam39433560 on 4/21/16.
 */
public class Receta implements Serializable {


    private int id_user;
    private String nombre;
    private String dificultad;
    private int tiempo;
    private String explicacion;
    private int id_receta;
    private Map<String, Integer> lista_ingredientes;

    //Constructor con todos los atributos.
    public Receta(int id_user, String nombre, String dificultad, int tiempo, String explicacion, int id_receta, Map<String, Integer> lista_ingredientes) {
        this.id_user = id_user;
        this.nombre = nombre;
        this.dificultad = dificultad;
        this.tiempo = tiempo;
        this.explicacion = explicacion;
        this.id_receta = id_receta;
        this.lista_ingredientes = lista_ingredientes;
    }

    //Constructor de prueba.
    public Receta(String nombre) {
        this.nombre = nombre;
    }



    public int getId() {
        return id_receta;
    }

    public void setId(int id) {
        this.id_receta = id;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDificultad() {
		return dificultad;
	}

	public void setDificultad(String dificultad) {
		this.dificultad = dificultad;
	}

	public int getTiempo() {
        return tiempo;
    }

    public void setTiempo(int tiempo) {
        this.tiempo = tiempo;
    }

    public String getExplicacion() {
        return explicacion;
    }

    public void setExplicacion(String explicacion) {
        this.explicacion = explicacion;
    }

    public int getId_receta() {
        return id_receta;
    }

    public void setId_receta(int id_receta) {
        this.id_receta = id_receta;
    }

    public Map<String, Integer> getLista_ingredientes() {
        return lista_ingredientes;
    }

    public void setLista_ingredientes(Map<String, Integer> lista_ingredientes) {
        this.lista_ingredientes = lista_ingredientes;
    }

    @Override
    public String toString() {
        return "Receta{" +
                "id_user=" + id_user +
                ", nombre='" + nombre + '\'' +
                ", dificultad=" + dificultad +
                ", tiempo=" + tiempo +
                ", explicacion='" + explicacion + '\'' +
                '}';
    }
}

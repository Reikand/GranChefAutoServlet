package Servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;

import Clases.Usuario;
import DAO.DAOUsers;

/**
 * Servlet implementation class ServletRegistroUsuario
 */
@WebServlet(description = "Servlet que se usara para el registro de usuarios en la base de datos", urlPatterns = { "/ServletRegistroUsuario" })
public class ServletRegistroUsuario extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletRegistroUsuario() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json");
		response.setCharacterEncoding("utf-8");
		PrintWriter out = response.getWriter();
		JSONObject resp = new JSONObject(); 
		
		//Recuperamos el Json que con la informacion del usuario.
		String objectJson = request.getParameterValues("Json")[0];
		Gson gson = new Gson();
		//Reconstruimos el objeto usuario desde el Json.
		Usuario user = gson.fromJson(objectJson, Usuario.class);
		user.setUserName(user.getEmail());
		
		//Introducimos el usuario en la base de datos.
		DAOUsers daouser = new DAOUsers();
		String result = daouser.addUser(user);
		
		//Respuesta del servidor al movil.
		if (result.equals("-1")) {
			try {
				resp.put("codigo", 400);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			try {
				resp.put("codigo", 200);
				resp.put("usuario", result);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		System.out.println(resp.toString());
		out.print(resp.toString());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
				
		
		
	}

}

package Servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;

import Clases.Receta;
import DAO.DAORecetas;

/**
 * Servlet implementation class ServletIntroducirReceta
 */
@WebServlet("/ServletIntroducirReceta")
public class ServletIntroducirReceta extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletIntroducirReceta() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("application/json");
		response.setCharacterEncoding("utf-8");
		PrintWriter out = response.getWriter();
		Gson gson = new Gson();
		JSONObject resp = new JSONObject();
		
		//Recuperamos los datos en formato Json.
		String recipe = request.getParameterValues("receta")[0];
		
		//Generamos los objetos usando la libreria Gson.
		Receta receta = gson.fromJson(recipe, Receta.class);
//		Type typeOfHashMap = new TypeToken<Map<String, String>>().getType();
		//Solo necesario para recuperar las listas de ingredientes a la hora de buscar recetas.
//		Map<String, Integer> listaIng = gson.fromJson(ing, Map.class);
		
		System.out.println(receta.toString());
//		System.out.println(ing);
	
		DAORecetas daorecetas = new DAORecetas();
		int result = daorecetas.addReceta(receta);
		
		if (result != -1) {
			//Si el resultado de introducir la receta el diferente de -1 enviaremos un Json
			//al App cliente que le indicara al usuario si se ha introducido bien.
			try {
				resp.put("codigo", "200");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			try {
				resp.put("codigo", "400");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		out.print(resp.toString());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
		
	}

}

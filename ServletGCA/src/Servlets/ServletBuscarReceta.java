package Servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;

import Clases.Receta;
import DAO.DAORecetas;

/**
 * Servlet implementation class ServletBucarReceta
 */
@WebServlet("/ServletBuscarReceta")
public class ServletBuscarReceta extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ServletBuscarReceta() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("application/json");
		response.setCharacterEncoding("utf-8");
		PrintWriter out = response.getWriter();
		Gson gson = new Gson();
		JSONObject resp = new JSONObject();

		// Recuperamos el Json con la lista de los ingredientes y sus
		// cantidades.
		String lista_ingredientes = request.getParameterValues("ingredientes")[0];
		 System.out.println(lista_ingredientes);

		// Llamada al metodo que buscara las recetas mas convenientes segun los
		// ingredientes que se tengan.
		DAORecetas daorecetas = new DAORecetas();
		List<Receta> lista_recetas = daorecetas.buscarReceta(lista_ingredientes);

		// Informacion de retorno al cliente.
		//Pasamos la lista con las recetas a Json para enviarlo de nuevo al cliente.
		String lista = gson.toJson(lista_recetas);
		
		System.out.println(lista);
		
		try {
			resp.put("lista_recetas", lista);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		out.print(resp.toString());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

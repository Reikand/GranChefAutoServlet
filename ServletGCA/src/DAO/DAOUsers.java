package DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.google.gson.Gson;

import Clases.Usuario;
import Connection.Conexion;

public class DAOUsers {
	
	Conexion con = new Conexion();
	
	/*
	 * Comprueba en la base de datos si el email introducido ya esta registrado.
	 * Falta arreglar el cerrar la conexion para que no se cierre.
	 */
	public boolean comprobarUsuario(String email) {

		PreparedStatement st;
		String query = "SELECT count(*) FROM usuarios WHERE email='" + email + "'";

		try {
			st = con.getConexion().prepareStatement(query);
			ResultSet rs = st.executeQuery();
			rs.next();
			if (rs.getInt(1) == 1) {
				System.out.println(rs.getInt(1) + " Correo existente");
				return false;
			}
			System.out.println(rs.getInt(1));
			rs.close();
			st.close();
//			con.desconectar();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Correo no existente");
		return true;
	}
	
	public int getIdUser(Usuario user) {

		PreparedStatement st;
		int result = 0;
		String query = "SELECT id FROM usuarios WHERE email='" + user.getEmail() + "', password='" + user.getPassword() + "'";

		try {
			st = con.getConexion().prepareStatement(query);
			ResultSet rs = st.executeQuery();
			rs.next();
			result = rs.getInt(1);
			rs.close();
			st.close();
//			con.desconectar();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		return result;
	}
	
	/*
	 * Metodo que a�ade un nuevo usuario en la Base de datos.
	 */
	public String addUser(Usuario user) {
		
		PreparedStatement st;
		String query = "INSERT INTO usuarios (email, userName, password) VALUES (?,?,?)";
		
		if (comprobarUsuario(user.getEmail())) {
			try {
				st = con.getConexion().prepareStatement(query);
				st.setString(1, user.getEmail());
				st.setString(2, user.getUserName());
				st.setString(3, user.getPassword());
				
				if (!st.execute()) {
					System.out.println("Usuario registrado con exito");
					int idUsuario = getIdUser(user);
					user.setId(idUsuario);
					Gson gson = new Gson();
					String usuarioReg = gson.toJson(user);
					System.out.println(usuarioReg.toString());
					return usuarioReg;				
				}
			}catch (SQLException ex) {
				ex.printStackTrace();
				ex.getMessage();
			}
		}
		return "-1";
	}

	

	/*
	 * LOGIN METHODS
	 */
	
	public boolean comprobarEmailPass(String email, String pass) {
		
		Statement st;
		String query = "SELECT count(*) FROM usuarios WHERE email='" + email + "' and password='" + pass + "'";
		
		try {
			st = con.getConexion().createStatement();
			ResultSet rs = st.executeQuery(query);
			rs.next();
			if (rs.getInt(1) == 1) {
				System.out.println("Coincidencia!!");
				return true;
			}
			rs.close();
			st.close();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		return false;
	}

	public String loginUser(String email, String pass) {
		
		Usuario user;
		Statement st;
		ResultSet rs;
		String query = "SELECT * FROM usuarios WHERE email='" + email + "' and password='" + pass + "'";
		String result = null;
		
//		boolean status = comprobarEmailPass(email, pass);
		
//		if (con == null) {
//			con = new Conexion();
//		}
		
		try {
			if (comprobarEmailPass(email, pass)) {
				st = con.getConexion().createStatement();
				rs = st.executeQuery(query);
				rs.next();
				user = new Usuario(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4));
				Gson gson = new Gson();
				result = gson.toJson(user);
				System.out.println(result.toString());
			} else {
				return "-1";
			}
			rs.close();
			st.close();
			con.desconectar();
			
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		return result;
	}
	
}

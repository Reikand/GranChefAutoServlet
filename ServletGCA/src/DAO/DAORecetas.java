package DAO;

import java.lang.reflect.Type;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import Clases.Receta;
import Connection.Conexion;

public class DAORecetas {
	
	Conexion con = new Conexion();
	
	public int addReceta(Receta receta) {
	
		PreparedStatement st = null;
		String query = "INSERT INTO recetas(id_user, nombre, dificultad, tiempo, explicacion, ingredientes) VALUES(?,?,?,?,?,?)";
		Gson gson = new Gson();
		String listaIngredientes = gson.toJson(receta.getLista_ingredientes());
		
		try {
			st = con.getConexion().prepareStatement(query);
			st.setInt(1, receta.getId_user());
			st.setString(2, receta.getNombre());
			st.setString(3, receta.getDificultad());
			st.setInt(4, receta.getTiempo());
			st.setString(5, receta.getExplicacion());
			st.setString(6, listaIngredientes);
			
			int resultado = st.executeUpdate();
			if (resultado != -1) {
				//Falta a�adir las fotografias de cada receta.
				System.out.println("Receta introducida correctamente!");
				return 1;
			} else {
				System.out.println("Problema al introducir la Receta");
			}			
			st.close();
			con.desconectar();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		return -1;
	}
	
	//===================================================
	//		METODOS BUSCAR RECETAS
	//===================================================
	
	/*
	 * Metodo que retornara una lista de recetas segun los ingredientes que introduzcas.
	 */
	public List<Receta> buscarReceta(String lista_ingredientes) {

		PreparedStatement st = null;
		ResultSet rs = null;
		String query = "SELECT id, ingredientes FROM recetas";
		// Map donde guardaremos el id y la cantidad de coincidencias con las
		// recetas guardadas.
		Map<Integer, Integer> lista_recetas = new TreeMap<Integer, Integer>();
		Type typeOfHashMap = new TypeToken<HashMap<String, Integer>>() {
		}.getType();

		Gson gson = new Gson();
		Map<String, Integer> listaIng = null;
		Map<String, Integer> listaIngredientes = gson.fromJson(lista_ingredientes, typeOfHashMap);

		try {
			// Ejecutamos la query para que nos retorne los datos en forma de
			// tabla de todas las recetas de la base de datos.
			st = con.getConexion().prepareStatement(query);
			rs = st.executeQuery();

			// Vamos recorriendo cada fila de la tabla
			while (rs.next()) {
				// Sacamos el id de la receta que estamos mirando y sus
				// ingredientes.
				int id_receta = rs.getInt("id");
				String ingredient = rs.getString("ingredientes");
				System.out.println(ingredient);
				if (!ingredient.equals("{}")) {
					listaIng = gson.fromJson(ingredient, typeOfHashMap);
				}
				// Inicializamos el contador de repeticione de ingredientes.
				int repeticiones = 0;
				// Hacemos un bucle para que vaya comprobando si el ingrediente
				// esta en la lista de los ingredientes sacada de la db.
				Iterator it = listaIngredientes.keySet().iterator();
				while (it.hasNext()) {
					// Sacamos el ingrediente con el iterator.
					String ingrediente = (String) it.next();
					if (listaIng.containsKey(ingrediente)) {
						// Si la lista que hemos sacado de la base de datos
						// contiene el ingrediente sumaremos uno al contador de
						// repeticiones.
						repeticiones++;
					}
				}
				// Una vez ya a comprobado todos los ingredientes guardara en un
				// nuevo Map el id de la receta y la cantidad de repeticiones.
				if (repeticiones > 0) {
					System.out.println(id_receta + " --- " + repeticiones);
					listaIng.clear();
					lista_recetas.put(id_receta, repeticiones);
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		 List<Receta> listaRecetas = listRecetas(lista_recetas);
		 return listaRecetas;
	}
	
	public List<Receta> listRecetas(Map<Integer, Integer> listaId) {
		
		List<Receta> listaFinalRecetas = new ArrayList<Receta>();
		
		Gson gson = new Gson();
		Statement st;
		ResultSet rs;
		String query = "";
		
		try {
			//Iniciamos la conexion con la base de datos.
			st = con.getConexion().createStatement();
			//Recorremos todas las posiciones del Map para buscar en la base de datos las id's de las recetas de la db y recontruirlas.
			Iterator it = listaId.keySet().iterator();
			while (it.hasNext()) {
				int id = (int) it.next();
				query = "SELECT * FROM recetas WHERE id=" + id;
				rs = st.executeQuery(query);
				rs.next();
				Type typeOfHashMap = new TypeToken<HashMap<String, Integer>>(){}.getType();
				Map<String, Integer> lista_ing = gson.fromJson(rs.getString(7), typeOfHashMap);
 				Receta receta = new Receta(rs.getInt("id_user"), rs.getString("nombre"), rs.getString("dificultad"), rs.getInt("tiempo"), rs.getString("explicacion"), rs.getInt("id"), lista_ing);
				listaFinalRecetas.add(receta);				
			}
			
		}catch (SQLException e) {
			e.printStackTrace();
		}		
		return listaFinalRecetas;
	}
	
	
//	Mostramos el contenido del map para ver que se guarda bien (prueba).
//  Iterator it = mapIngredientes.keySet().iterator();
//  while(it.hasNext()){
//      Ingrediente key = (Ingrediente)it.next();
//      System.out.println("Ingrediente: " + key.getNameIngredient() + " -> Cantidad: " + mapIngredientes.get(key));
//  }
	
	
//	try {
//	st = con.getConexion().prepareStatement(query);
//	ResultSet rs = st.executeQuery();
//	while (rs.next()) {
//		int id_receta = rs.getInt(1);
//		Map<String, Integer> listaIng = gson.fromJson(rs.getString(2), Map.class);
//		int repeticiones = 0;
//		Iterator it = listaIngredientes.keySet().iterator();
//		while(it.hasNext()) {
//			String key = (String)it.next();
//			if (listaIng.containsKey(key)) {
//				repeticiones++;
//			}
//		}
//		lista_recetas.put(id_receta, repeticiones);
//	}
//	st.close();
//	con.desconectar();			
//} catch (SQLException ex) {
//	ex.printStackTrace();
//}

}
